// ==UserScript==
// @name         関西大学図書館予約資料受取希望館自動入力
// @description  関西大学図書館のOPACで予約資料の受取希望館を自動入力
// @version      1.2
// @match        https://www.lib.kansai-u.ac.jp/webopac/rsvexm.do*
// @author       atpsaym
// @namespace    http://bitbucket.org/atpsaym/
// @license      CC0
// ==/UserScript==

var pickupLib = localStorage.getItem("__pickup_lib__");
if(pickupLib){
    var input = document.querySelector("input[name='hopar'][value='" + pickupLib + "']");
    input && input.click();
}

var submitButton = document.querySelector(".opac_footer_btn_area .opac_imgbtn");
submitButton.addEventListener("click", function(){
    localStorage.setItem("__pickup_lib__", document.querySelector("input[name='hopar']:checked").value);
});
