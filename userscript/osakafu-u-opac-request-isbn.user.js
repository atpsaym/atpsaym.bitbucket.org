// ==UserScript==
// @name         大阪府立大学図書館学生希望図書リクエスト書誌情報入力支援
// @description  ISBNを元に国立国会図書館サーチのAPIから書誌情報を取得して自動入力する
// @version      1.1
// @match        https://opac.osakafu-u.ac.jp/opac-service/srv_bok_req.php*
// @author       atpsaym
// @namespace    http://bitbucket.org/atpsaym/
// @license      CC0
// ==/UserScript==

if(document.querySelector('.srv_title').textContent == '学生希望図書リクエスト'){
    var $isbn = document.querySelector('input[name="ISBN"]');
    var $button = document.createElement('button');
    var buttonText = $button.textContent = '書誌情報取得';
    $button.addEventListener('click', setBiblioData);
    $isbn.parentNode.appendChild($button);
}

function setBiblioData(){
    var isbn = $isbn.value;
    $button.disabled = true;
    $button.textContent = '読み込み中';

    GM_xmlhttpRequest({
        method: 'GET',
        url: 'http://iss.ndl.go.jp/api/sru?operation=searchRetrieve&recordSchema=dcndl&query=isbn=' + isbn,
        onload: function(response){
            var domParser = new DOMParser();
            var $responseXML = domParser.parseFromString(response.responseText, 'text/xml');
            var $record = domParser.parseFromString($responseXML.querySelector('recordData').textContent, 'text/xml');
            var $resource = $record.getElementsByTagNameNS('http://ndl.go.jp/dcndl/terms/', 'BibResource')[0];

            setBookInfo({
                title: xpathTexts('./dcterms:title', $resource).join(' / '),
                author: xpathTexts('./dc:creator', $resource).join(', '),
                publisher: xpathTexts('./dcterms:publisher/foaf:Agent/foaf:name', $resource).join(', '),
                issuedYear: xpathTexts('./dcterms:issued', $resource).toString(),
                price: xpathTexts('./dcndl:price', $resource).toString().replace(/円$/, '')
            });

            $button.disabled = false;
            $button.textContent = buttonText;
        },
        onerror: function(){
            $button.disabled = false;
            $button.textContent = '失敗しました';
        }
    });
}

function setBookInfo(info){
    var table = {
        title: 'TR',
        author: 'AL',
        publisher: 'PUB',
        issuedYear: 'PYEAR',
        price: 'PLANPRI'
    };
    for(var key in info){
        if(!table[key]) continue;
        var $input = document.querySelector('input[name="' + table[key] + '"]');
        $input.value = info[key];
    }
}

function xpathTexts(xpath, contextNode){
    var document = contextNode.ownerDocument;
    var nsResolver = document.createNSResolver(document.documentElement);
    var result = document.evaluate(xpath, contextNode, nsResolver, XPathResult.ANY_TYPE, null);
    var tmp, ret = [];
    while(tmp = result.iterateNext()){
        ret.push(tmp.textContent);
    }
    return ret;
}
