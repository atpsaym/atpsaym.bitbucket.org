// ==UserScript==
// @name         OPU OPAC Title
// @description  大阪府立大学図書館のOPACの書誌ページのタイトルに書名と著者名を挿入
// @version      1.0
// @match        https://opac.osakafu-u.ac.jp/opac/opac_details.cgi*
// @namespace    https://bitbucket.org/atpsaym/
// @author       atpsaym
// @license      CC0
// ==/UserScript==

document.title = document.querySelector("#bb_ttl").textContent + " - 大阪府立大学図書館";
